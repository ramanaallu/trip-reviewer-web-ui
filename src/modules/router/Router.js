import {includes} from 'lodash/collection';

let deps = [
  'ui.router'
];

let getDefaultView = (templateUrl) => {
  return {
    "lazyLoadView": {
      templateUrl: templateUrl
    }
  };
};

let getChildBaseView = () => {
  return {
    "lazyLoadView": {
      template: '<div class=""><div ui-view></div></div>'
    }
  };
};

export default angular.module('Router', deps)
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('home', {
        url: '/',
        views: getDefaultView('modules/home/home.html'),
        controller: ['$rootScope', function($rootScope) {
              	$rootScope.TitleService.reset();
              	$rootScope.MetaService.reset();
        }]
      }).state('hotels', {
        url: '/hotels',
        views: getChildBaseView(),
        resolve: {
          loadHotels: ['$q', '$ocLazyLoad', function($q, $ocLazyLoad) {
            let deferred = $q.defer();
            require.ensure([], function() {
              let module = require('../hotels/Hotels');
              $ocLazyLoad.load({
                name: 'Hotels'
              });
              deferred.resolve(module);
            });
            return deferred.promise;
          }]
        }
      })
      .state('hotels.search', {
        url: "/search?debug=true&where&in&out&id&type&rooms&numadults&postalCode&location",
        templateUrl: "modules/hotels/hotel-list.html",
        controller: ['$scope', '$rootScope', function($scope, $rootScope) {
              	$rootScope.TitleService.reset();
              	$rootScope.MetaService.reset();
              	$rootScope.$emit('skim-search', 'fromHotel');
              	$scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState)=> {
	              	//if (includes(['', 'hotels.search'], fromState.name)) {
	              		$scope.$broadcast('search-hotels', toParams);
	              	//}
              	});
        }],
        params: {originalSearch: null}
      })
      .state('hotels.popular', {
        url: "/:trigger/:where/id/:id",
        templateUrl: "modules/popular-destinations/popular-hotel.html",
        controller: ['$scope', '$location', '$anchorScroll' ,'$rootScope', function($scope, $location, $anchorScroll,$rootScope) {
          $rootScope.$emit('skim-search', 'fromHotel');
          $scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState)=> {
            	 $scope.$broadcast('search-hotels', toParams);
          });
          $scope.scrollTo = function(id) {
        	    $location.hash(id);
        	    $anchorScroll();
        	  }
        }],
        params: {originalSearch: null}
      }).state('hotels.populars', {
          url: "/:where/id/:id",
          templateUrl: "modules/hotels/hotel-list.html",
          controller: ['$scope', '$location', '$anchorScroll' ,'$rootScope', function($scope, $location, $anchorScroll,$rootScope) {
            $rootScope.$emit('skim-search', 'fromHotel');
            $scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState)=> {
              	toParams.trigger = 'hotels'; 
            	$scope.$broadcast('search-hotels', toParams);
            });
            $scope.scrollTo = function(id) {
          	    $location.hash(id);
          	    $anchorScroll();
          	  }
          }],
          params: {originalSearch: null}
        })
      .state('static', {
        url: '/static',
        views: getChildBaseView(),
        data: {
          requireLogin: false
        }
      })
      .state('static.about', {
        url: '/about',
        templateUrl: 'modules/static/about.html',
        controller:	['$rootScope', function($rootScope) {
        	$rootScope.TitleService.setTitle('Best Hotel Review site in United States- Tripreviewer.com');
        	//$rootScope.MetaService.reset();
        	$rootScope.MetaService.setMetaDescription('From luxury hotel to budget hotel, Tripreviewer has the best deals and discounts for hotels in USA.');
        	$rootScope.MetaService.appendMetaKeywords('Hotel details, Trip reviewer about us page, details about hotels, about Tripreviewer, hotel'
        			+'reviews , Trip reviewer , travel advisor, hotel rating , review hotels , hotel star ratings, hotel trip,' 
        			+'hotel rating system, reviews of hotels, hotel star rating, best hotel review sites, hotel advisor, '
        			+'travel hotel reviews, travel port view trip, Tripreviewer about us page, reviews and rates for '
        			+'Hotels, reviews and rates for Hotels & Restaurants, online portal for travelers, reviews and '
        			+'prices of hotel');
          }]
      })
      .state('static.faq', {
          url: '/faq',
          templateUrl: 'modules/static/faq.html',
          controller:	['$rootScope', function($rootScope) {
            	$rootScope.TitleService.reset();
            	$rootScope.MetaService.reset();
           }]
      })
      .state('static.privacy', {
        url: '/privacy',
        templateUrl: 'modules/static/privacy.html',
        controller:	['$rootScope', function($rootScope) {
          	$rootScope.TitleService.reset();
          	$rootScope.MetaService.reset();
         }]
      })
      .state('static.copyright', {
        url: '/copyright',
        templateUrl: 'modules/static/copyright.html',
        controller:	['$rootScope', function($rootScope) {
          	$rootScope.TitleService.reset();
          	$rootScope.MetaService.reset();
         }]
      })
      .state('static.terms', {
        url: '/terms',
        templateUrl: 'modules/static/terms.html',
        controller:	['$rootScope', function($rootScope) {
          	$rootScope.TitleService.reset();
          	$rootScope.MetaService.reset();
         }]
      })
      .state('static.manageReservations', {
        url: '/manage-reservations',
        templateUrl: 'modules/static/manage-reservations.html',
        controller:	['$rootScope', function($rootScope) {
          	$rootScope.TitleService.reset();
          	$rootScope.MetaService.reset();
         }]
      })
      .state('static.contact', {
        url: '/contact',
        templateUrl: 'modules/static/contact.html',
        controller:	['$rootScope', function($rootScope) {
        	$rootScope.TitleService.setTitle('Contact us | Tripreviewer.com');
        	//$rootScope.MetaService.reset();
        	$rootScope.MetaService.setMetaDescription('Tripreviewer.com has 24/7 customer care support. We provide the details about all types of hotels in US and their prices.');
        	$rootScope.MetaService.appendMetaKeywords('Hotel numbers, hotel contact number, hotel telephone numbers, Tripreviewer contact number, contact Tripreviewer, Tripreviewer contact page');
          }]
      });

  }]);
