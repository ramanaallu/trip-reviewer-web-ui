import  './popular-destinations/PopularDestinations';
import './hotels/Hotels';

let deps = [
  'ngSanitize',
  'matchMedia',
  'mgcrea.ngStrap.datepicker',
  'isteven-multi-select',
  'dmCarrousel',
  'ui.select',
  'oc.lazyLoad',
  'PopularDestinations',
  'Hotels'
];
export default angular.module('TRModules', deps);