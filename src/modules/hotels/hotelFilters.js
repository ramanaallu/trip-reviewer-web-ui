import {any, map, contains} from 'lodash/collection';
import {isEmpty} from 'lodash/lang';
import {inRange} from 'lodash/number';

let ratingFilter = (hotel, hotelFilter)  => {
  return hotel.starRating >= hotelFilter.rating;
};

let ratingTRFilter = (hotel, hotelFilter)  => {
  if(hotelFilter.trrating == 0){
    return true;
  }else
  {
    return ((hotel.averageRating/10 > hotelFilter.trrating-1) && (hotel.averageRating/10 <= hotelFilter.trrating));
    //return hotel.averageRating/10 == hotelFilter.trrating
  }
};

let locationFilter = (hotel, hotelFilter) => {
  let selectedLocations = map(hotelFilter.selectedLocations, (location) => {
    return location.location.toLocaleLowerCase();
  });
  if (!selectedLocations.length || !hotel.location) {
    return true;
  }
  return contains(selectedLocations, hotel.location.toLowerCase());
};

let amenityFilter = (hotel, hotelFilter) => {
	  let selectedAmenities = map(hotelFilter.selectedAmenities, (amenity) => {
	    return amenity.name.toLocaleLowerCase();
	  });
	  if (!selectedAmenities.length || hotel.amenities && !hotel.amenities.length) {
	    return true;
	  }
	  return any(hotel.amenities, (amenity)=> {
	    return contains(selectedAmenities, amenity.toLowerCase());
	  });
};
	
let hasPriceFilter = (item, hotelFilter)  => {
  return hotelFilter.price;
};

let hotelNameFilter = (item, hotelFilter)  => {
	return hotelFilter.hotelName == '' ? true : item.name.toLowerCase().indexOf(hotelFilter.hotelName.toLowerCase()) !== -1 ? true:false;
};

let xsPriceFilter = (item, hotelFilter)  => {
  //console.log("xs", !hotelFilter.price.xs || item.price < 90);
  return !hotelFilter.price.xs || item.price < 90;
};

let smPriceFilter = (item, hotelFilter)  => {
  //console.log("sm", !hotelFilter.price.sm || (item.price > 90 && item.price < 150));
  return !hotelFilter.price.sm || inRange(item.price, 90, 150);
};

let mdPriceFilter = (item, hotelFilter)  => {
  //console.log("md", !hotelFilter.price.md || (item.price > 150 && item.price < 260));
  return !hotelFilter.price.md || inRange(item.price, 150, 260);
};

let lgPriceFilter = (item, hotelFilter) => {
  //console.log("lg", !hotelFilter.price.lg || item.price > 260);
  return !hotelFilter.price.lg || item.price > 260;
};

export function filterHotels(hotel, hotelFilter) {
 	return hasPriceFilter(hotel, hotelFilter) &&
 	ratingTRFilter(hotel, hotelFilter) &&
    ratingFilter(hotel, hotelFilter) &&
    amenityFilter(hotel, hotelFilter) &&
    xsPriceFilter(hotel, hotelFilter) &&
    smPriceFilter(hotel, hotelFilter) &&
    mdPriceFilter(hotel, hotelFilter) &&
    lgPriceFilter(hotel, hotelFilter) && 
    hotelNameFilter(hotel,hotelFilter)&& 
    locationFilter(hotel, hotelFilter);
}