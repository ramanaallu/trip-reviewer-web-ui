class IcePortalPhotosController {

  constructor($scope, iceUrl, $uibModalInstance) {
    this.$scope = $scope;
    this.$scope.iceUrl = iceUrl;

    $scope.closeMap = function() {
      $uibModalInstance.close();
    };
  }
}

IcePortalPhotosController.$inject = ['$scope', 'iceUrl', '$uibModalInstance'];

export default IcePortalPhotosController;
