import {take, takeRight, last, first} from 'lodash/array';
import {floor,round, min, sum} from 'lodash/math';
import {partial, delay} from 'lodash/function';
import {keys, has, pick} from 'lodash/object';
import {capitalize} from 'lodash/string';
import {each, reduce, find, contains, map, sortBy, sortByOrder} from 'lodash/collection';
import logger from '../util/logger';

import * as HotelFilters from './hotelFilters';
const IMAGES = {
  google: "/images/check_google.png",
  getaroom: "/images/getaroom.png",
  bdc: "/images/booking.com.png",
  etb: "/images/ETB-Logo-Orange.png",
  yelp: "/images/yelp_review.png",
  foursquare: "/images/foursquare.png",
  expedia: "/images/check_expedia.png",
  tripReviewer: "/images/check_tripreviewer.png",
  tripadvisor: "/images/check_tripadvisor.png",
  "tripadvisor-rating": "/images/trip-advisor-rating.gif",
  derbysoft:"/images/derbysoft.jpg",
  trustyou:"/images/trustyou.png",
  hoteltravel:"/images/hoteltravel.gif"
};

let findDataURI = (dataUris, uriKey)=> {
  return find(dataUris, (dataUri)=> {
    return contains(dataUri, uriKey);
  });
};

let calculateAverageRating = (hotel) => {
  let ratings = [];
  each(keys(hotel.readReviews), (key) => {
    let review = hotel.readReviews[key];
    if (review.rating) {
      ratings.push(review.rating);
    }
  });
  let totalRating = sum(ratings) / ratings.length;
  var floorTotalRating = floor(totalRating);
  let averageRating = floorTotalRating + (round((totalRating - floorTotalRating)) ? 0.5 : 0.0 );
  hotel.averageRating = averageRating * 10;
};

class HotelSearchController {

  constructor($scope, $rootScope, $timeout, $uibModal, TRService) {
    this.$timeout = $timeout;
    this.$uibModal = $uibModal;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.TRService = TRService;
    this.reset();
    this.loadAmenities();
    $scope.priceFilters = this.TRService.loadPriceFilters();
    $scope.selectedAmenities = [];
    $scope.selectedLocations = [];
    $scope.locations = [];
    $scope.hotelNameSearch = "";
    $scope.priceFilterSelected = null;
    $scope.itemsPerPage = 5;
    $scope.currentPage = 1;

    this.$scope.markers = {};
    
    $scope.basePath = './modules/popular-destinations/';   
    $scope.$watch("hotelFilter", () => {
      this.evaluateHotelVisibility();
    }, true);
    
    $scope.amenitiesFilter = (which,flag)=>{
    	if(flag){ 
    		$scope.selectedAmenities.push(which);
    	}else{
    		$scope.selectedAmenities.pop(which);
    	}
    	 $scope.hotelFilter.selectedAmenities = $scope.selectedAmenities;
    }

    $scope.classNameForProgressBar = (reviewScore) => {
      var className = 'progress-bar';
       className = className + ( (reviewScore >= 80) ? ' progress-bar-success' :
                        (reviewScore >= 50 && reviewScore < 80) ? ' progress-bar-warning' :
                        (reviewScore < 50 ) ? ' progress-bar-danger' : "" );
      return  className;
    }

    $scope.locationFilter = (which,flag)=>{
    	if(flag){ 
    		$scope.selectedLocations.push(which);
    	}else{
    		$scope.selectedLocations.pop(which);
    	}
    	 $scope.hotelFilter.selectedLocations = $scope.selectedLocations;
    }
    
    $scope.hotelNameFilter = (which)=>{
    	if(which && which.length >=3 ){
    		$scope.hotelFilter.hotelName = which;
    	}else if(!which && which.length == 0){
    		$scope.hotelFilter.hotelName = "";
    	}
    };
   
    
    $scope.priceFilter = (which,flag)=> {
    	$scope.priceFilterSelected = which;
    	this.restPriceFilters();
      let filter = $scope.hotelFilter.price[which.name];
      $timeout(()=> {
        $scope.hotelFilter.price[which.name] = !filter;
      });
    };

    $scope.$on('search-hotels', (obj, criteria) => {
    	logger.log("originalSearch: ", criteria.originalSearch);
      $scope.searchCriteria = criteria.originalSearch;
      $scope.searchString = TRService.getFormattedSearchString(criteria);
      if(criteria.trigger && criteria.trigger === 'popular-hotels'){
    	  criteria.type = 'CITIES';
    	  criteria.numberofadults = 2;
    	  criteria.numadults = 2;
    	  criteria.numberofchildren = 0;
    	  criteria.rooms = 1;
    	  criteria.in = moment().format('l');
    	  criteria.out = moment().add(1, 'days').format('l');
    	  criteria.maxrec = 10;
    	  criteria.where = criteria.where.replace(new RegExp('-', 'g'),' ');
    	  $scope.city1 = $scope.basePath + criteria.where.replace(new RegExp(' ', 'g'),'-')+"-doc1.html";
    	  $scope.city2 = $scope.basePath + criteria.where.replace(new RegExp(' ', 'g'),'-')+"-doc2.html";
    	  $scope.cityName = criteria.where.replace(new RegExp('-', 'g'),' ')
    	  $rootScope.$emit('set-search-criteria', criteria);
      }else if(criteria.trigger && criteria.trigger === 'hotels'){
    	  criteria.maxrec = -1;
    	  criteria.type = 'CITIES';
    	  criteria.numberofadults = 2;
    	  criteria.numadults = 2;
    	  criteria.numberofchildren = 0;
    	  criteria.rooms = 1;
    	  criteria.in = moment().format('l');
    	  criteria.out = moment().add(1, 'days').format('l');
    	  criteria.where = criteria.where.replace(new RegExp('-', 'g'),' ');
    	  $rootScope.$emit('set-search-criteria', criteria);
      }else{
    	  $rootScope.$emit('set-search-criteria', criteria);
      }
      this.searchHotels(criteria);
    });

    let clearPopupVisibility = ()=> {
      each($scope.hotelList, (hotel)=> {
        each(keys(hotel.popUpList), (key)=> {
          hotel.popUpList[key] = false;
        });
        hotel.popupVisible = false;
      });

    };
    $scope.trackGA = (key, hotelName) =>{
      ga('send', 'event', key, 'Click', hotelName, 4);
    }
    $scope.togglePopup = (hotel, key) => {
      ga('send', 'event', key, 'Click', hotel.name, 4);
      if(key === 'readReviews'){
    	  //this.sortReviews(hotel.readReviews,'rating',false);
      }
      let oldValue = hotel.popUpList[key];
      clearPopupVisibility(hotel);
      hotel.popupVisible = !oldValue;
      hotel.popUpList[key] = !oldValue;
      $scope.ratesPerPage = null;
      $scope.currentPage = 1;
      if (hotel.popupVisible) {
        if (key === 'restaurants') {
          this.loadRestaurants(hotel);
        }
        if (key === 'rates') {
          hotel.rates = sortBy(hotel.rates, 'roomPrice');
          $scope.ratesPerPage = hotel.rates.slice(0,$scope.itemsPerPage);
        }
      }
    };

    $scope.loadMore = (hotel) => {
      var newItems = hotel.rates.slice($scope.currentPage*$scope.itemsPerPage,
                                      ($scope.currentPage*$scope.itemsPerPage)+$scope.itemsPerPage);
      $scope.ratesPerPage = $scope.ratesPerPage.concat(newItems);
      $scope.currentPage++;
    };

    $scope.nextPageDisabledClass = function() {
      return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.pageCount = function() {
      return Math.ceil($scope.total/$scope.itemsPerPage);
    };

    $scope.closePopup = (hotel)=> {
      $scope.currentPage = 0;
      clearPopupVisibility(hotel);
    };

    $scope.clearFilters = () => {
     $("#hotelNameInput").val("");
      $timeout(()=> {
        this.resetFilter();
        //this.resetHotelAmenities();
      });
    };

    $scope.showReview = (hotel, reviewType) => {
      $scope.trackGA('Read Reviews', reviewType)
      each(keys(hotel.readReviews), (rt) => {
        let k = (rt + "-" + hotel.id);
        $scope.reviews[k] = false;
      });

      let key = (reviewType + "-" + hotel.id);
      $scope.reviews[key] = true;

    };

    $scope.canShowReview = (hotelId, reviewType) => {
      let key = (reviewType + "-" + hotelId);
      return $scope.reviews[key];
    };

    $scope.sortHotels = (item) => {
      var sortOrder = $scope.sortList[item];
      this.resetSortList();
      $scope.sortList[item] = !sortOrder;
      this.sortHotels(item, $scope.sortList[item] ? 'asc' : 'desc');
    };

    $scope.reviewToolTip = (hotel) => {
      let reviews = map(hotel.readReviews, (review, key) => {
        return '<span class="left">' + key + '</span>' +
          '<span class="right">' + review.rating + '</span>';
      });
      return reviews.join('<br/>');
    };
    
    $scope.highlightMarker = (index,event) => {
	        	if(event == 'mouseover')
	        	    this.$scope.markers[index].setIcon('../images/blue_blank.png');
	        	else if(event == 'mouseout')
	        		this.$scope.markers[index].setIcon('../images/green_blank.png');	
    	   
      };
      
      

    $scope.scrollToTop = () => {
      $("div.demo").scrollTop(300);
    };

    $scope.toggleSearch = () => {
      $scope.showSearch = !$scope.showSearch;
      $rootScope.$emit('set-search-criteria', $scope.searchCriteria);
    };

    $scope.capitalize = (key) => {
      return capitalize(key);
    };

    $scope.showPhotos = (photos, id) => {
      this.showPhotos(photos, id)
    };

    $scope.showMap = (hotel) => {
      this.showMap(hotel);
    };
    
    $scope.resetSelectedAmenities = (flag) => {
    	if(!flag){
    		this.resetSelectedAmenities();
    		this.$scope.hotelFilter.selectedAmenities = '';
    	}
    }
    $scope.resetHotelName = (flag) => {
    	if(!flag){
    		$("#hotelNameInput").val("");
    		this.$scope.hotelFilter.hotelName = '';
    	}
    }
    
    $scope.resetRating = (flag) => {
    	if(!flag){
    		this.$scope.hotelFilter.rating = 0;
    		this.$scope.hotelFilter.max = 5;
    	}
    }

    $scope.resetTRRating = (flag) => {
        if(!flag){
            this.$scope.hotelFilter.trrating = 0;
            this.$scope.hotelFilter.trmax = 5;
        }
    }

    $scope.resetSelectedLocations = (flag) => {
    	if(!flag){
    		this.resetSelectedLocations();
    		this.$scope.hotelFilter.selectedLocations = '';
    	}
    }
    
    $scope.resetSelectedPrice = (flag) => {
    	if(!flag){
    		this.resetSelectedPrice();
    		this.restPriceFilters();
    		this.$scope.hotelFilter.price = {};
    	}
    }

  }

  getIcon() {
	    return MapIconMaker.createMarkerIcon({width: 20, height: 34, primaryColor: color, cornercolor:color});
}
  
  showMap(hotel) {
    let mapInfo = pick(hotel, ['latitude', 'longitude']);
    this.$uibModal.open({
      animation: true,
      templateUrl: 'map.html',
      size: 'lg',
      controller: 'MapController',
      resolve: {
        mapInfo: () => {
          return mapInfo;
        }
      }
    });
  }

  showPhotos(photos, id) {
    let handleSuccess = (response)  => {
        let url = response[0].url; //"https://www.iceportal.com/brochures/ice/Brochure.aspx?did=13070&brochureid=ICE20320";
        if (url !== undefined) {
          this.$uibModal.open({
            animation: true,
            templateUrl: 'iceportalphotos.html',
            size: 'lg',
            controller: 'IcePortalPhotosController',
            resolve: {
              iceUrl: () => {
                return url;
              }
            }
          });
        } else {
          showEANPhotos(photos);
        }
    }

    let handleFailure = ()=> {
      showEANPhotos(photos);
    };
    let url = this.TRService.getIcePortalUrl(id).then(handleSuccess, handleFailure);
  }

  showEANPhotos(photos){
    this.$uibModal.open({
      animation: true,
      templateUrl: 'photos.html',
      size: 'md',
      controller: 'PhotoController',
      resolve: {
        photos: () => {
          return photos;
        }
      }
    });
  }

  sortHotels(attribute, order) {
    this.$timeout(() => {
      this.$scope.hotelList = sortByOrder(this.$scope.hotelList, attribute, order);
    });
  }

  loadRestaurants(hotel) {
    let handleSuccess = (data) => {
      hotel.nearByRestaurants = map(data.restaurantDetails, (detail) => {
        //let detailURL = "http://tripreviewer.com/restaurants/index.html#restaurantDetails" + detail.detailUrl.replace('?', ':');
        let detailURL = "http://tripreviewer.com/restaurants/index.html#restaurantDetails/services/restaurant/detail:country=US&restaurantId=" + detail.id;
        return {
          name: detail.name,
          cuisineType: detail.cuisineType,
          detailUrl: detailURL,
          image: detail.images && detail.images.length ? detail.images[0] : 'images/no-image.gif'
        };
      });
      this.$scope.nearByRestaurantsProgress = false;
    };
    this.$scope.nearByRestaurantsProgress = true;
    this.TRService.loadRestaurants(hotel.latitude, hotel.longitude)
      .then(handleSuccess)
      .catch(() => {
        this.$scope.nearByRestaurantsProgress = false;
      });
  }

  evaluateHotelVisibility() {
    each(this.$scope.hotelList, (hotel)=> {
        hotel.visible = hotel.price && HotelFilters.filterHotels(hotel, this.$scope.hotelFilter);
    });

    this.$scope.filteredCount = reduce(this.$scope.hotelList, function(total, hotel) {
      let count = hotel.visible ? 1 : 0;
      return total + count;
    }, 0);
  }

  loadExternalDataPaginated(providerKey, dataURI, handleResultsCallback) {

    let handleSuccess = (originalURI, response)=> {
      if (response) {
        let data = response.data;
        logger.end(originalURI);

        logger.log(providerKey + '-data:', data);
        handleResultsCallback(data);

        if (data.moreResultsAvailable === true && data.nextPageLocation) {
          let nextPageURI = data.nextPageLocation;
          if (nextPageURI) {
            getData(nextPageURI);
          }
        } else if (response.status === 202) {
          //delay(partial(getData, originalURI), 300);
          getData(originalURI);
        }
      }
    };

    let handleFailure = (response) => {
      logger.log(providerKey + " Failure ==>", response && response.status);
    };

    let getData = (uri) => {
      logger.start(uri);
      this.TRService.loadExternalHotelData(uri).then(partial(handleSuccess, uri), handleFailure);
    };

    dataURI && getData(dataURI);
  }

  searchGoogle(dataUris) {
    let providerKey = "google";
    let updateData = (googleData) => {
      each(googleData.hotelReviewSummaries, (reviewData)=> {
        let hotel = find(this.$scope.hotelList, (h) => {
          return h.id === reviewData.hotelId;
        });
        HotelSearchController.updateReviewData(providerKey, hotel, reviewData);
      });
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchYelp(dataUris) {
    let providerKey = "yelp";

    let updateData = (googleData) => {
      each(googleData.hotelReviewSummaries, (reviewData)=> {
        let hotel = find(this.$scope.hotelList, (h) => {
          return h.id === reviewData.hotelId;
        });
        HotelSearchController.updateReviewData(providerKey, hotel, reviewData);
      });
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  static updateRateData(hotelList, hotelRates, providerKey) {

  each(hotelList, (h) => {
    each(hotelRates, (hotelRate)=> {

        if(hotelRate.hotelId === h.id)
        {
          //console.log(h.id +" - "+ providerKey +"-"+ hotelRate.rate.roomPrice);
          HotelSearchController.setHotelPrices(h, hotelRate.rate, providerKey);

        }
      });
      //HotelSearchController.setHotelPrices(hotel, hotelRate.rate, providerKey);
    });
  }

  static setHotelPrices(hotel, hotelRate, providerKey) {
    if (hotel && hotelRate) {
    	    hotel.prices.push(hotelRate.roomPrice);
	    hotel.price = min(hotel.prices);
	    var icon = IMAGES[providerKey === 'expedia' ? 'tripReviewer' : providerKey];
           hotel.rates.push({
               icon: icon,
               roomDescription: hotelRate.roomDescription,
               roomPrice: hotelRate.roomPrice,
               totalPrice: hotelRate.totalPrice,
               refund: hotelRate.refund,
               providerURL: hotelRate.providerUrl,
               providerKey: providerKey,
           });
    }
  }

  static updateReviewData(providerKey, hotel, reviewData) {
   let summary = reviewData.reviewSummary;
    if (hotel && summary) {
      let icon = summary.icon ? summary.icon : IMAGES[providerKey];
      if (providerKey === 'tripadvisor') {
        icon = summary.ratingIcon;
      }

      hotel.writeReviews[providerKey] = {
        icon: IMAGES[providerKey],
        reviewWriteURL: summary.reviewWriteURL
      };
      hotel.readReviews[providerKey] = {
        icon: icon,
        rating: summary.rating,
        reviews: summary.reviewContents,
        providerURL: summary.providerUrl,
        reviewType: summary.reviewType ? summary.reviewType : ""
      };
      calculateAverageRating(hotel);
    }
  }
  searchFourSquare(dataUris) {
    let providerKey = "foursquare";
    let updateData = (fourSquareData) => {
      each(fourSquareData.hotelReviewSummaries, (reviewData)=> {
        let hotel = find(this.$scope.hotelList, (h) => {
          return h.id === reviewData.hotelId;
        });
        if (hotel && reviewData.reviewSummary) {
          hotel[providerKey] = {
            totalVisitors: reviewData.reviewSummary.usersCount,
            totalVisits: reviewData.reviewSummary.checkInsCount
          };
        }
        HotelSearchController.updateReviewData(providerKey, hotel, reviewData);
      });
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchBookingDotCom(dataUris) {
    let providerKey = "bdc";
    let updateData = (data) => {
      HotelSearchController.updateRateData(this.$scope.hotelList, data.hotelRates, providerKey);
      each(data.hotelRates, (hotelRate)=> {
            let hotel = find(this.$scope.hotelList, (h) => {
              return h.id === hotelRate.hotelId;
            });
            let reviewSummary = {"rating":hotelRate.rate.reviewScore === "0.0" ? "" : hotelRate.rate.reviewScore,
            					 "reviewWriteURL" : hotelRate.rate.reviews,
            					 "providerUrl" : hotelRate.rate.reviews};
            hotelRate.reviewSummary = reviewSummary;
            HotelSearchController.updateReviewData(providerKey, hotel, hotelRate);
      });
      //this.evaluateHotelVisibility();
    };
    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchGetARoom(dataUris) {
    let providerKey = "getaroom";
    let updateData = (data) => {
      HotelSearchController.updateRateData(this.$scope.hotelList, data.hotelRates, providerKey);
      //this.evaluateHotelVisibility();
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchEasyToBook(dataUris) {
    let providerKey = "etb";
    let updateData = (data) => {
      HotelSearchController.updateRateData(this.$scope.hotelList, data.hotelRates, providerKey);
      //this.evaluateHotelVisibility();
    };
    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchExpedia(dataUris) {
    let providerKey = "expedia";
    let updateData = (expediaData) => {
      each(expediaData.hotelRateAndReviews, (ratingData)=> {
        let hotel = find(this.$scope.hotelList, (h) => {
          return h.id === ratingData.hotelId;
        });
        if (hotel) {
          each(ratingData.rates, (rate)=> {
            HotelSearchController.setHotelPrices(hotel, rate, providerKey);
          });

          let expediaReview = first(ratingData.hotelReviewSummaries);
          if (expediaReview && expediaReview.reviewSummary) {
            hotel.readReviews[providerKey] = {
              icon: '/images/check_expedia.png',
              providerURL: expediaReview.reviewSummary.reviewWriteURL
            };
          }
          HotelSearchController.updateReviewData('tripadvisor', hotel, ratingData.hotelReviewSummaries[1]);
        }
      });
      this.evaluateHotelVisibility();
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  //TODO: Added as part of Derby Soft integration.
  searchDerbySoftData(dataUris) {
    let providerKey = "derbysoft";
    let updateData = (data) => {
      HotelSearchController.updateRateData(this.$scope.hotelList, data.hotelRates, providerKey);
      //this.evaluateHotelVisibility();
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
    //this.loadExternalDataPaginated(providerKey, "derbysoft/4c228745c996eb4cfce40def1b82164779c02edd", updateData)
  }

  //Added as part of Trustyou integration.
  searchTrustyou(dataUris) {
  	let providerKey = "trustyou";
    let updateData = (trustyouData) => {
      each(trustyouData.hotelReviewSummaries, (reviewData)=> {
        let hotel = find(this.$scope.hotelList, (h) => {
          return h.id === reviewData.hotelId;
        });
        HotelSearchController.updateReviewData(providerKey, hotel, reviewData);
        //this.evaluateHotelVisibility();
      });
    };

    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  //Added as part of HotelTravel Integration.
  searchHotelTravel(dataUris) {
    let providerKey = "hoteltravel";
    let updateData = (data) => {
      HotelSearchController.updateRateData(this.$scope.hotelList, data.hotelRates, providerKey);
      //this.evaluateHotelVisibility();
    };
    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchWeatherData(dataUris) {
    let providerKey = "weather";
    let updateData = (data) => {
      this.$scope.weatherData = data && data.weatherDetails;
    };
    let dataURI = findDataURI(dataUris, providerKey);
    this.loadExternalDataPaginated(providerKey, dataURI, updateData)
  }

  searchHotels(criteria) {
    let incrementHotelsToShow = () => {
      //logger.log("Show more hotels: " + $scope.hotelsToShow);
      this.$scope.hotelsToShow = this.$scope.hotelsToShow + 10;
    };

    let handleSuccess = (results) => {
      var dataUris = results.dataUris;
      this.searchExpedia(dataUris);
      this.searchFourSquare(dataUris);
      this.searchGoogle(dataUris);
      this.searchYelp(dataUris);
      this.searchBookingDotCom(dataUris);
      this.searchGetARoom(dataUris);
      this.searchEasyToBook(dataUris);
      this.searchWeatherData(dataUris);
      //this.searchDerbySoftData(dataUris);
      this.searchTrustyou(dataUris);
      this.searchHotelTravel(dataUris);

      let hotels = this.$rootScope.mobile ? results.hotelProperties.slice(0, 100) : results.hotelProperties;
      let hotelList = each(hotels, (hotel)=> {
        //hotel.visible = true;
        hotel.detailURL = this.TRService.getHotelDetailURL(criteria, hotel.id);
        hotel.prices = [];
        var location = {location:hotel.location,
        				ticked:false};

        if(!this.checkArray(location)){
        	this.$scope.locations.push(location);
        }
        hotel.writeReviews = {};
        hotel.readReviews = {};
        hotel.rates = [];
        hotel.popUpList = {
          photos: false,
          readReviews: false,
          writeReviews: false,
          restaurants: false,
          rates: false
        };
        hotel["photos"] = reduce(hotel.images, (result, value)=> {
          result.push({
            path: value
          });
          return result;
        }, []);
      });
      this.$scope.hotelList = hotelList;
      this.evaluateHotelVisibility();

      let loopingFunction = () => {
        this.$timeout(incrementHotelsToShow());
        if (this.$scope.hotelsToShow < hotelList.length) {
          this.$timeout(loopingFunction);
        }
      };

      loopingFunction();
      this.showProgress(false);
      if(criteria.trigger === 'popular-hotels'){
    	  this.plotMap();
       }
    };
    let handleFailure = ()=> {
      this.showProgress(false);
    };

    logger.start("searchHotels");
    this.showProgress(true);
    this.TRService.searchHotels(criteria).then(handleSuccess, handleFailure);
  }
  checkArray(obj){
	  var i = this.$scope.locations.length;
	    while (i--) {
	       if (this.$scope.locations[i].location === obj.location) {
	           return true;
	       }
	    }
	    return false;
  }
  showProgress(progress) {
    if(progress)
    {
      this.$scope.searchProgress = progress;
    }
    else
    {
      this.$timeout(()=> {
        this.$scope.searchProgress = progress;
      }, 100);
    }
  }

  restPriceFilters() {
    this.$scope.hotelFilter.price = {
      xs: false,
      sm: false,
      md: false,
      lg: false
    }
  }

  resetFilter() {
    this.$scope.hotelFilter = {
      rating: 0,
      max: 5,
      trrating: 0,
      trmax: 5,
      selectedAmenities: '',
      price: {},
      hotelName:'',
      selectedLocations : ''
    };
    this.restPriceFilters();
    this.resetSelectedAmenities();
    this.resetSelectedLocations();
    this.resetSelectedPrice();
 }
  
  resetSelectedAmenities(){
  	  each(this.$scope.selectedAmenities, (amenity)=> {
  		  amenity.ticked = false;
  	  });
  	  this.$scope.selectedAmenities = [];
    }
    
   resetSelectedLocations(){
  	  each(this.$scope.selectedLocations, (location)=> {
  		  location.ticked = false;
  	  });
  	  this.$scope.selectedLocations = [];
    }
    
    resetSelectedPrice(){
  	  if(this.$scope.priceFilterSelected){
  		  this.$scope.priceFilterSelected.ticked = '';
  	  }
    }
  resetHotelAmenities(amenities) {
    this.$scope.amenities = sortBy(map(amenities, (amenity) => {
      return {
        name: amenity,
        marker: amenity,
        ticked: false
      };
    }), 'name');
    
    
  }

  loadAmenities() {
    this.resetHotelAmenities(this.TRService.loadAmenities());
  }

  resetSortList() {
    this.$scope.sortList = {};
  }

  reset() {
    this.$scope.showSearch = false;
    this.$scope.hotelsToShow = 20;
    this.$scope.hotelList = [];
    this.$scope.reviews = {};
    this.resetSortList();
    this.resetFilter();
  }
  
   plotMap(){
	 var labelIndex = 1;
	  var myLatLng = {lat: this.$scope.hotelList[0].latitude, lng: this.$scope.hotelList[0].longitude};
	  var map = new google.maps.Map(document.getElementById("plotMapDiv"), {
		    center: myLatLng,
		    zoom: 13
      });
	  let hotelList = each(this.$scope.hotelList, (hotel)=> {
		  var infowindow = new google.maps.InfoWindow({
			content: hotel.name 
		  });
		  var marker = new google.maps.Marker({
			    record_id : "marker"+labelIndex,  
			    position: {lat: hotel.latitude, lng: hotel.longitude},
			    map: map,
			    size: new google.maps.Size(100, 75),
			    title: hotel.name,
			    label:labelIndex+"",
			    icon : '../images/green_blank.png'
			  });
		  marker.addListener('mouseover', function() {
			    //infowindow.open(map, marker);
			    $("#hotel"+marker.label).attr('class', 'location-icon-hover cboxElement');
		  });
		  marker.addListener('mouseout', function() {
			    //infowindow.close();
			    $("#hotel"+marker.label).attr('class', 'location-icon1 cboxElement');
		  });
		  this.$scope.markers[labelIndex] = marker;
		  labelIndex++;
	 });
  }

}

HotelSearchController.$inject = ['$scope', '$rootScope', '$timeout', '$uibModal', 'TRService'];

export default HotelSearchController;