class PhotoController {

  constructor($scope, photos, $uibModalInstance) {
    this.$scope = $scope;
    this.$scope.photos = photos;

    $scope.closePhotos = function() {
      $uibModalInstance.close();
    };
  }

}

PhotoController.$inject = ['$scope', 'photos', '$uibModalInstance'];

export default PhotoController;