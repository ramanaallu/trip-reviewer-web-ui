class HotelSearchController {

  constructor($scope, $timeout, $rootScope, StateUtils, TRService, $modal, $document) {
    this.$scope = $scope;
    this.reset();
    $rootScope.TitleService.reset();
    $rootScope.MetaService.reset();
    $rootScope.showFeedback = false;
    $scope.showError = false;
    $rootScope.$on('skim-search', (obj, source) => {
      this.$scope.searchMode = 'small';
    });

    $rootScope.$on('set-search-criteria', (obj, criteria) => {
      if (criteria) {
    	if(criteria.trigger && criteria.trigger === 'popular-hotels'){
    		var cityObject = {
    				targetId : criteria.id,
    				value : criteria.where.replace(new RegExp('-', 'g'),' '),
    				type : criteria.type
    		}
    		//$scope.hotelSearch.city.targetId = criteria.id;
    		$scope.hotelSearch.city = cityObject;//criteria.where.replace(new RegExp('-', 'g'),' ');
    		//$scope.hotelSearch.city.type = criteria.type;
    		$scope.hotelSearch.in = criteria.in;
    		$scope.hotelSearch.out = criteria.out;
    		$scope.hotelSearch.rooms = criteria.rooms;
    		$scope.hotelSearch.numberofadults = criteria.numberofadults;
    		$rootScope.TitleService.setTitle($rootScope.params.title);
    		$rootScope.MetaService.setMetaDescription($rootScope.params.metaDesc);
    		$rootScope.MetaService.appendMetaKeywords($rootScope.params.keywords);
    	}else if(criteria.trigger && criteria.trigger === 'hotels'){
    		var cityObject = {
    				targetId : criteria.id,
    				value : criteria.where.replace(new RegExp('-', 'g'),' '),
    				type : criteria.type
    		}
    		//$scope.hotelSearch.city.targetId = criteria.id;
    		$scope.hotelSearch.city = cityObject;//criteria.where.replace(new RegExp('-', 'g'),' ');
    		//$scope.hotelSearch.city.type = criteria.type;
    		$scope.hotelSearch.in = criteria.in;
    		$scope.hotelSearch.out = criteria.out;
    		$scope.hotelSearch.rooms = criteria.rooms;
    		$scope.hotelSearch.numberofadults = criteria.numberofadults;
    	} else{
    		$scope.hotelSearch = criteria;
    	}  
      }
    });

    $scope.disableError = function(){
    	$scope.showError = false;
    }
    $scope.suggestHotels = (q)=> {
      let handleSuccess = (data) => {
        $scope.hotels = data.suggestions ? data.suggestions : [];
      };

      return TRService.suggestHotels(q).then(handleSuccess);
    };

    $scope.$watch('hotelSearch.in', (newValue) => {
      if (newValue) {
        $scope.hotelSearch.out = moment(new Date(newValue)).add(1, 'days').format('l');
      } else {
        $scope.hotelSearch.in = moment().format('l');
      }
    });

    $scope.$watch('hotelSearch.out', (newValue) => {
      if (!newValue) {
        $scope.hotelSearch.out = moment(new Date($scope.hotelSearch.in)).add(1, 'days').format('l');
      } else {
        if(moment(new Date($scope.hotelSearch.out)) <= moment(new Date($scope.hotelSearch.in))){
          $scope.hotelSearch.in = moment(new Date($scope.hotelSearch.out)).format('l');
        }
      }
    });

    $scope.groupByHotelType = (item) => {
      return item.type;
    };

    $scope.search = () => {
      var data = $scope.hotelSearch;
      $scope.showError = false;
      if(!data.city){
    	  $scope.showError = true;
    	  return false;
      }
      let formatDate = (date)=> {
        return moment(new Date(date)).format('l');
      };
      let searchData = {
        where: data.city.value,
        in: decodeURIComponent(formatDate(data.in)),
        out: decodeURIComponent(formatDate(data.out)),
        id: data.city.targetId,
        type: data.city.type,
        "numberofadults": data.numberofadults,
        "numberofchildren": data.numberofchildren,
        "rooms": data.rooms
      };
      StateUtils.showHotels(searchData, data);
    };

    var TimeOutTimerValue = 300000;
    var TimeOut_Thread = $timeout(function(){ LogoutByTimer() } , TimeOutTimerValue);
    var bodyElement = angular.element($document);

    angular.forEach(['keydown', 'keyup', 'click', 'mousemove', 'DOMMouseScroll', 'mousewheel', 'mousedown', 'touchstart', 'touchmove', 'scroll', 'focus'],

    function(EventName) {
        bodyElement.bind(EventName, function (e) { TimeOut_Resetter(e) });
    });

    function LogoutByTimer(){
        $modal.open({
         templateUrl:'/modules/hotels/popup.html'
       });
    }

    function TimeOut_Resetter(e){
        /// Stop the pending timeout
        $timeout.cancel(TimeOut_Thread);
        /// Reset the timeout
        TimeOut_Thread = $timeout(function(){ LogoutByTimer() } , TimeOutTimerValue);
    }
  }

  resetDates() {
    let today = moment().format('l');
    let tomorrow = moment().add(1, 'days').format('l');
    this.$scope.hotelSearch['in'] = today;
    this.$scope.hotelSearch['out'] = tomorrow;

  }

  reset() {
    this.$scope.searchMode = 'large';
    this.$scope.hotelSearch = {
      rooms: 1,
      numberofadults: 2
    };
    this.resetDates();
    this.$scope.hotels = [];

  }

}

HotelSearchController.$inject = ['$scope', '$timeout', '$rootScope', 'StateUtils', 'TRService', '$modal', '$document'];

export default HotelSearchController;