var title;
class PageTitle
{
	constructor(){
		this.reset();
	} 
	reset(){
		title = 'Find Hotel Reviews| Online booking hotels- Tripreviewer.com';
	}
	title(){
		return title; 
	}
	
	setTitle(newTitle){
		 title = newTitle;
	}
}

PageTitle.$inject = [];

export default PageTitle;
