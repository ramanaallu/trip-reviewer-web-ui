import env from '../util/env';
import {template, templateSettings} from 'lodash/string';
import {merge} from 'lodash/object';
import {last} from 'lodash/array';
import {times} from 'lodash/utility';
templateSettings.interpolate = /{{([\s\S]+?)}}/g;
import logger from '../util/logger';

const HOTEL_DETAIL_URL = '/restaurants/index.html#hotelDetails/services/hotel/detail:checkin={{checkIn}}&checkout={{checkOut}}&hotelId={{hotelId}}&{{roomsURLPart}}';

// Trip API
const HOTEL_SEARCH = '/hotel/search';
const HOTEL_SUGGEST = '/suggest/hotels?term={{q}}';
const HOTEL_RESTAURANT_LIST = '/restaurant/list';
const HOTEL_AMENITIES = '/hotel/amenities';
const ICEPORTAL_URL = '/hotel/searchIce?hotelId={{id}}';

//Rating API
const HOTEL_EXTERNAL = '/hotel/{{searchToken}}?mock=false';

let getURL = (url, mockURL)=> {
  return env.getServiceURL(url, mockURL);
};

class TRService {
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
  }

  searchHotels(data) {
    let url = getURL(HOTEL_SEARCH, 'mocks/hotel-list.json');
    let requestData = {
      name: data.where,
      checkIn: decodeURIComponent(data.in),
      checkout: decodeURIComponent(data.out),
      destinationid: data.id,
      destinationtype: data.type,
      'numberofadults': data.numberofadults,
      'numberofchildren': data.numberofchildren,
      'rooms': data.rooms,
      'maxrec' : data.maxrec
    };
    return this.$http.get(url, {
      params: requestData
    }).then((response) => {
      return response.data;
    });
  }

  suggestHotels(q) {
    var url = getURL(template(HOTEL_SUGGEST)({q: q}),'');
    return this.$http.get(url).then((response) => {
      return response.data;
    });
  }

  loadExternalHotelData(searchToken) {
    let deferred = this.$q.defer();
    let url = getURL(template(HOTEL_EXTERNAL)({searchToken: searchToken}));
    let options = {timeout: deferred.promise, cancel: deferred};
    let handleSuccess = (response)=> {
      deferred.resolve(response);
    };
    let handleError = (response)=> {
      deferred.reject(response);
    };

    this.$http.get(url, options).then(handleSuccess, handleError);
    return deferred.promise;
  }

  getHotelDetailURL(criteria, hotelId) {
    let roomsURLPart = times(criteria.rooms, (n) => {
      return template("room[{{index}}].numberOfAdults={{numberOfAdults}}")({
        index: n,
        numberOfAdults: criteria.numadults
      });
    });

    return template(HOTEL_DETAIL_URL)({
      checkIn: criteria.in,
      checkOut: criteria.out,
      hotelId: hotelId,
      roomsURLPart: roomsURLPart.join('&')
    });
  }

  loadRestaurants(lat, lng) {
    let data = {
      "latitude": lat,
      "longitude": lng,
      "pagination": {"pageSize": 6, "currentPageNumber": 1}
    };
    var url = env.getTripReviewerURL(HOTEL_RESTAURANT_LIST, 'mocks/restaurant-list.json');
    return this.$http.post(url, data).then((response) => {
      return response.data;
    });
  }

  getIcePortalUrl(id) {
    let url = getURL(template(ICEPORTAL_URL)({id: id}), ' ');
    return this.$http.get(url).then((response) => {
      return response.data;
    });
  }

  getFormattedSearchString(searchCriteria) {
    let format = '{{location}}; {{cin}} - {{cout}}; Rooms: {{rooms}}; Nights: {{nights}}';
    let checkin = moment(new Date(decodeURIComponent(searchCriteria.in)));
    let checkout = moment(new Date(decodeURIComponent(searchCriteria.out)));
    let nights = checkout.diff(checkin, 'days');
    let input = {
      location: searchCriteria.where,
      cin: checkin.format('MM/D'),
      cout: checkout.format('MM/D'),
      rooms: searchCriteria.rooms,
      nights: nights
    };
    return template(format)(input);
  }

  getMapURL(mapInfo){
    let mapURL = "https://maps.google.com/maps?q={{latitude}},{{longitude}}&ie=UTF8&t=m&output=embed";

    return template(mapURL)(mapInfo);
  }
  loadAmenities() {
    return [
      "Business Center",
      "Fitness Center",
      "Smoke Free",
      "Free Wifi",
      "Pets Allowed",
      "Pool",
      "Elevator/Lift",
      "Wheelchair Accessible",
      "Microwave/Fridge",
      "Free Breakfast"
    ];
  }

  loadPriceFilters(){
	  return[
	         {"name": 'xs', "description":'below $90','ticked':''},
	         {"name": 'sm', "description":'$90 - $150','ticked':''},
	         {"name": 'md', "description":'$150 - $260','ticked':''},
	         {"name": 'lg', "description":'higher then $260','ticked':''}
	         ];
  }

}

TRService.$inject = ['$http', '$q'];

export default TRService;