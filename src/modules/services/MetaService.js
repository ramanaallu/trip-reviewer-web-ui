var metaDescription = '';
var metaKeywords = '';
class MetaService
{
	constructor(){
		this.reset();
	} 
	
	metaDescription(){ 
    	return metaDescription; 
    }
    
    metaKeywords(){ 
    	return metaKeywords; 
    }
    reset(){
    	metaDescription = 'Tripreviewer can help you find the best hotels in the USA.' 
			+'On Tripreviewer, you can read honest reviews, compare prices, to view current offers and book online.';
		metaKeywords = 'Tripreviewer, Tripreviewer hotel bookings , Tripreviewer hotel ratings, best hotels in the USA,'
			+'best hotel rooms, hotel reviewer, trip reviews, best hotels in us, travel reviews ,travel advisor,'
			+'luxury hotels and resorts,  American best hotel ,travel review websites, best hotel reviews, small '
			+'luxury hotels ,best family hotel ,hotel review website, best hotels in USA, best hotel review sites,' 
			+'booking hotels, hotel accommodations , hotels in America, hotel rating system, booking a hotel' 
			+'room , hotel room prices, best hotel in USA.';
    }
    setMetaDescription(newMetaDescription){
        metaDescription = newMetaDescription;
    }
    appendMetaKeywords(newKeywords){
        metaKeywords = newKeywords;
    }
}

MetaService.$inject = [];

export default MetaService;
