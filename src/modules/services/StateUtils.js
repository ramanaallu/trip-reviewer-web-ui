import {template, templateSettings, escape} from 'lodash/string';
templateSettings.interpolate = /{{([\s\S]+?)}}/g;

const HOTEL_URL = 'search?where={{where}}&in={{in1}}&out={{out}}';

class StateUtils {

  constructor($rootScope, $state, $timeout) {
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$timeout = $timeout;
  }

  showHotels(criteria, originalSearch) {
    debugger;
	  let rooms = criteria.rooms ? criteria.rooms : 1;
    let numAdults = criteria.numberofadults ? criteria.numberofadults : 2;
    if (originalSearch) {
      originalSearch.numberofadults = numAdults;
      originalSearch.rooms = rooms;
      originalSearch.city = {
        targetId: criteria.id,
        type: criteria.type,
        value: criteria.where ? criteria.where : criteria.city
      };
    }

    let params = {
      where: criteria.where,
      in: criteria.in,
      out: criteria.out,
      id: criteria.id,
      type: criteria.type,
      rooms: rooms,
      numadults: numAdults,
      originalSearch: originalSearch
    };

    this.navigate('hotels.search', params);
  }
  
  showPopularHotels(criteria) {
	    let rooms = criteria.rooms ? criteria.rooms : 1;
	    let numAdults = criteria.numberofadults ? criteria.numberofadults : 2;
	    let params = {
	      where: criteria.where.replace(new RegExp(' ', 'g'),'-'),
	      in: criteria.in,
	      out: criteria.out,
	      id: criteria.id,
	      type: criteria.type,
	      rooms: rooms,
	      numadults: numAdults,
	      trigger:criteria.trigger,
	      title:criteria.title,
	      metaDesc:criteria.metaDesc,
	      keywords:criteria.keywords
	    };
	    this.$rootScope.params = params;
	    this.navigate('hotels.popular', params);
	  }

  navigate(path, params = null) {
    this.$timeout(()=> {
      this.$state.go(path, params, {reload: false});
    });
  }

}

StateUtils.$inject = ['$rootScope', '$state', '$timeout'];

export default StateUtils;