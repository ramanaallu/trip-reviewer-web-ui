


import 'jquery';
import './bower_components/angular/angular';
import './bower_components/angular-animate/angular-animate.js';
import 'moment';
//import 'bootstrap/dist/js/bootstrap.js';

//angular-core

import 'bootstrap/dist/css/bootstrap.css';
import 'angular-ui-select/dist/select.css'
import './angular-strap/angular-strap.js';

// custom ui bootstrap carousel
//import './angular-bootstrap/ui-bootstrap-custom-0.14.0-csp.css';
import './angular-bootstrap/ui-bootstrap-custom-tpls-0.14.3.js';
import 'dm-angular-carrousel/dist/stylesheets/style.css';
import 'dm-angular-carrousel/dist/js/DmCarrousel.js';
import 'angular-media-queries/match-media';

import 'oclazyload/dist/ocLazyLoad';
import 'angular-cookies/angular-cookies';
import 'angular-ui-router/release/angular-ui-router';

import 'angular-sanitize/angular-sanitize.js'

import 'angular-ui-select/dist/select.js'

import './angular-multi-select/isteven-multi-select.css';
import './angular-multi-select/isteven-multi-select.js';

import './angular-bugsnag/angular-bugsnag.js';

