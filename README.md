# Trip Reviewer

Install NVM https://github.com/creationix/nvm#manual-install (follow manual install instructions).
This helps manage different versions of node and helps  install libraries and tools without requiring sudo access.
For windows environment, check https://github.com/coreybutler/nvm-windows.

##### Install the following node tools

```sh
$ npm install gulp -g
$ npm install bower -g
$ npm install webpack -g
```

### Download dependencies and start the app (First time only)
> npm start will download required dependencies, clean, lint, run tests and build the app and exports the output to /dist directory

```sh
$ npm install
$ bower install
```

### Local Development

Run gulp build for the first time (or when doing a clean build). Then just run gulp which will watch for changes.
Just running gulp bootstraps the project and watches for changes in html, CSS, Images and JS files.

```sh
$ gulp build
$ gulp

```


### Build For debug/dev (unminified)

```sh
$ gulp build

```

### Build For Production  (minified)

```sh
$ gulp build --ENV=prod

```

### VENDOR FILES (look at /vendor folder)

Vendor (/vendor folder) is another sub project that contains all the third party dependencies of the project. They are
build separately and comitted (we always commit the build output and git tag it for easy rollbacks if required).

The main projects' build task copies vendor output files required into its /dist folder and make them available for the
app to function as expected. Please refer to /vendor/README.md on how to build vendor.
