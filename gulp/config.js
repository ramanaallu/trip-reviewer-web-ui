var path = require('path');
var dest = './dist',
  src = './src';
module.exports = {
  browserSync: {
    server: {
      baseDir: [dest]
    },
    files: [
      dest + '/**'
    ],
    notify: false,
    open: false
  },
  markup: {
    src: src + "/**/*.html",
    dest: dest
  },
  less: {
    src: src + "/**/*.less",
    dest: dest
  },
  styles:[
    path.join(__dirname, '../src/**/*.css')
  ],
  images: {
    src: [
      path.join(__dirname, '../src/**/*.{eot,svg,ttf,woff,png,ico,jpg,gif}')
    ],
    dest: dest
  },
  mocks: {
    src: src + "/**/*.json",
    dest: dest
  },
  js: src + "/**/*.js",
  src: src,
  dest: dest,
  vendor: [
    path.join(__dirname, '../vendor/dist/*.{woff,woff2,svg,ttf,js}')
  ]
};
