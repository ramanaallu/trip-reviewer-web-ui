var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', function(cb) {

  runSequence([
      'clean',
      'webpack'
    ],

    'mocks',
    'styles',
    'images',
    'markup',
    'vendor');

  cb();
});
