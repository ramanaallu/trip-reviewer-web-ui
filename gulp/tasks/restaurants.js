var gulp = require('gulp');
var config = require('../config').restaurants;

gulp.task('restaurants', function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
